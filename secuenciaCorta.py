from __future__ import print_function
import sys

__author__ = 'miguegallardo16@hotmail.com'

from pyspark import SparkConf, SparkContext

if __name__ == "__main__":

    sConf = SparkConf()

    sCont = SparkContext(conf=sConf)

    sCont.setLogLevel("OFF")

    output = sCont \
        .textFile(sys.argv[1]) \
        .map(lambda line: (line.split(':')[0], len(line.split(':')[1]))) \
        .reduceByKey(lambda x, y: x + y) \
        .sortBy(lambda x: x[1], ascending=True) \
        .collect()

    for idx, (word, count) in enumerate(output):

        print("%s: %s" % (str(word).encode('utf-8'), str(count)))

        print(open(sys.argv[1], 'r').readlines()[int(word)].split(':')[1])

        break

    sCont.stop()
