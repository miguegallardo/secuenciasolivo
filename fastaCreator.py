__author__ = 'miguegallardo16@hotmail.com'

def comprobar(char):

	bases = ['A', 'C', 'G', 'T']

	if char in bases:
		return True
	return False

def main():

	f = open("secuenciasOlivo.txt", 'r').readlines()
	for idx, linea in enumerate(f):
		cadena = ""

		for char in linea:
			if comprobar(char):
				cadena += char

		with open('olivo.fasta', 'a') as fn:
			fn.write(str(idx) + ':' + cadena + '\n')

if __name__ == '__main__':
	main()